// Example program

#include <iostream>
#include <random>

using namespace std;

int main()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    
    float mean = 50; // set population mean
    float std_dev = 6;   //set standard deviation
    
    int n = 10; //set sample size
    
    std::normal_distribution<float> norm(mean,std_dev); //setup distribution object
	
	
    
    
    for (int x=0; x<n;x++)
	{
		float total= 0.0;
		float avg=0.0;
		float conf1=0.0;
		float conf2=0.0;
		
		cout<<endl<<"Sample number "<<x+1<<endl;
		for(int i = 0 ; i < 20; i++)// create a sample of size 5
		   {
			   float val = norm(gen);
			   cout<< val<<endl; // print out a random number to the screen
			   total = total + val;
				
			}
			
		avg= total/20;
		cout<<"The average is "<<avg<<endl;
		
		conf1= avg + ((1.96* std_dev)/sqrt(20));
		 conf2= avg - ((1.96* std_dev)/sqrt(20));
		cout<<"\n Positive Confidence level of: "<<conf1<<endl;
		cout<<"\n Negative Confidence level of: "<<conf2<<endl<<endl;
     } 
    
       
    
    
    //To Do: Create correct number of samples
    //       Calculate means for each sample
    //       (Calculate confidence intervals also??)
        
        
}
